import Foundation
import UIKit

final class CircularProgressBar: UIView {
    var width: CGFloat = 2

    var progress: CGFloat = 0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    private var backgroundLayer = CAShapeLayer()
    private var progressLayer = CAShapeLayer()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit() {
        backgroundLayer.lineWidth = width
        backgroundLayer.fillColor = nil
        backgroundLayer.strokeColor = UIColor.white.cgColor
        layer.addSublayer(backgroundLayer)

        progressLayer.lineWidth = width
        progressLayer.fillColor = nil
        layer.addSublayer(progressLayer)
        layer.transform = CATransform3DMakeRotation(CGFloat(90 * Double.pi / 180), 0, 0, -1)
    }

    override func draw(_ rect: CGRect) {
        let circle = UIBezierPath(ovalIn: rect.insetBy(dx: width / 2, dy: width / 2))
        backgroundLayer.path = circle.cgPath

        progressLayer.path = circle.cgPath
        progressLayer.strokeStart = 0
        progressLayer.strokeEnd = progress
        progressLayer.strokeColor = UIColor.red.cgColor
    }
}
