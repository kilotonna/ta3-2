import Foundation

import Foundation

struct DataContainer<T: Decodable>: Decodable {
    var data: T
}

struct SongModel: Codable {
    let id: String
    let name: String
    let audioURL: URL
}
