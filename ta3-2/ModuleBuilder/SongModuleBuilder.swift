import Foundation
import UIKit

final class SongModuleBuilder {
    func buildSongModule() -> UIViewController {
        let vc = SongViewController()
        
        let interactor = SongInteractor()
        
        vc.interactor = interactor
        vc.audioService = AudioService.shared
        
        return vc
    }
}
