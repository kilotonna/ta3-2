import UIKit

final class SongView: UICollectionViewCell {
    
    var viewModel: SongViewModel?
    
    private let songNameTitle = UILabel()
    private let downloadButton = UIButton()
    private let playPauseButton = UIButton()
    private let progressBar = CircularProgressBar()
    
    var downloadButtonHandler: ((SongViewModel) -> ())?
    var playPauseButtonHandler: ((SongViewModel) -> ())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        backgroundColor = UIColor.init(named: "grayMainColor")
        
        layer.cornerRadius = 8
        
        addSubview(songNameTitle)
        addSubview(downloadButton)
        addSubview(playPauseButton)
        addSubview(progressBar)
        
        songNameTitle.translatesAutoresizingMaskIntoConstraints = false
        downloadButton.translatesAutoresizingMaskIntoConstraints = false
        playPauseButton.translatesAutoresizingMaskIntoConstraints = false
        progressBar.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            songNameTitle.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            songNameTitle.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
            songNameTitle.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
            
            downloadButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
            downloadButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
            
            playPauseButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
            playPauseButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
            
            progressBar.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
            progressBar.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
            progressBar.heightAnchor.constraint(equalToConstant: 22),
            progressBar.widthAnchor.constraint(equalToConstant: 22)
        ])
        
        songNameTitle.font = UIFont.systemFont(ofSize: 26)
        songNameTitle.textColor = .white
        songNameTitle.numberOfLines = 0
        songNameTitle.lineBreakMode = .byWordWrapping
        
        downloadButton.setImage(UIImage.init(named: "download-icon"), for: .normal)
        downloadButton.addTarget(self, action: #selector(downloadBttonTap), for: .touchUpInside)
        
        playPauseButton.setImage(UIImage.init(named: "pause-icon"), for: .normal)
        playPauseButton.addTarget(self, action: #selector(playPauseButtonTap), for: .touchUpInside)
    }
    
    func configure(model: SongViewModel) {
        self.viewModel = model
        songNameTitle.text = model.name
        
        switch model.downloadState {
            
        case .none:
            downloadButton.isHidden = false
            playPauseButton.isHidden = true
            progressBar.isHidden = true
        case .progress:
            downloadButton.isHidden = true
            playPauseButton.isHidden = true
            progressBar.isHidden = false
            progressBar.progress = CGFloat(model.progress)
        case .done:
            downloadButton.isHidden = true
            playPauseButton.isHidden = false
            progressBar.isHidden = true
        }
        
        switch model.playerState {
        case .play:
            playPauseButton.setImage(UIImage.init(named: "play-icon"), for: .normal)
        case .stop:
            playPauseButton.setImage(UIImage.init(named: "pause-icon"), for: .normal)
        }
    }
    
    @objc
    func downloadBttonTap() {
        guard let viewModel = viewModel,
              let downloadButtonHandler = downloadButtonHandler else { return }
        downloadButtonHandler(viewModel)
    }
    
    @objc
    func playPauseButtonTap() {
        guard let viewModel = viewModel,
              let playPauseButtonHandler = playPauseButtonHandler else { return }
        
        playPauseButtonHandler(viewModel)
    }
}
