import Foundation

final class SongInteractor: NSObject {
    
    var downloadProgressHandler: ((TempModel) -> ())?
    
    private lazy var session: URLSession = {
        let config = URLSessionConfiguration.background(withIdentifier: "com.ta3-2.background")
        
        let queue = OperationQueue()
        queue.name = "downloadQueue"
        
        return URLSession(configuration: config, delegate: self, delegateQueue: queue)
    }()
    
    private var downloadService: DownloadService?
    private let fileService = FileService.shared
    
    override init() {
        super.init()
        
        downloadService = DownloadService(session: session)
    }
    
    func getSongList(_ completion: @escaping (_ data: [SongViewModel]?, _ error: Error?) -> ()) {
        if let path = Bundle.main.path(forResource: "mockNetworkData", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let decoder = JSONDecoder()
                let result = try decoder.decode(DataContainer<[SongModel]>.self, from: data)
                completion(result.data.map({ convert(songModel: $0) }), nil)
            } catch {
                print("Parse data error: \(error)")
                completion(nil, error)
            }
        }
    }
    
    func downloadSong(_ song: SongViewModel) {
        downloadService?.download(song)
        
        if let downloadProgressHandler = downloadProgressHandler,
           let downloadModel = downloadService?.activeTasks[song.audioURL] {
            downloadProgressHandler(downloadModel)
        }
    }
    
    private func convert(songModel: SongModel) -> SongViewModel {
        let model = SongViewModel(id: songModel.id,
                                  name: songModel.name,
                                  audioURL: songModel.audioURL)
        
        if let localFilePath = fileService.localFilePath(for: songModel.audioURL),
           fileService.isFileExist(localFilePath: localFilePath.path) {
            model.downloadState = .done
            model.audioLocalPath = localFilePath
        }
        
        return model
    }
}

extension SongInteractor: URLSessionDownloadDelegate, URLSessionDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        guard let url = downloadTask.originalRequest?.url,
              let downloadProgressHandler = downloadProgressHandler,
              let downloadModel = downloadService?.activeTasks[url] else { return }
        
        downloadService?.removeTask(by: url)
        
        guard let localFilePath = fileService.localFilePath(for: url) else { return }
        
        do {
            try FileManager.default.copyItem(at: location, to: localFilePath)
            downloadModel.state = .done
            downloadModel.localPath = localFilePath
            
            DispatchQueue.main.async {
                downloadProgressHandler(downloadModel)
            }
        } catch let error {
            print(error)
        }
        
        
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        
        guard let downloadService = downloadService,
              let downloadProgressHandler = downloadProgressHandler,
              let url = downloadTask.originalRequest?.url,
              let downloadModel = downloadService.activeTasks[url] else { return }
        
        downloadModel.progress = Double(totalBytesWritten) / Double(totalBytesExpectedToWrite)
        downloadModel.state = .progress
        
        DispatchQueue.main.async {
            downloadProgressHandler(downloadModel)
        }
    }
}
