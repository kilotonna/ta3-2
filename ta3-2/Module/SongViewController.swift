import UIKit
import AVFoundation

final class SongViewController: UIViewController {
    
    var interactor: SongInteractor?
    var audioService: AudioService?
    
    private var songList = [SongViewModel]()
    private var songsCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        setupCollectionView()
        updateData()
        
        interactor?.downloadProgressHandler = { [weak self] (model) in
            guard let self = self else { return }
            self.downloadProgressHandler(downloadModel: model)
        }
    }
    
    private func setupCollectionView() {
        let offset: CGFloat = 8
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: offset, left: offset, bottom: offset, right: offset)
        layout.itemSize = CGSize(width: view.frame.width - offset * 2, height: (view.frame.width - offset * 2) * 0.4)
        
        songsCollectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        songsCollectionView.register(SongView.self, forCellWithReuseIdentifier: "SongView")
        songsCollectionView.backgroundColor = .white
        view.addSubview(songsCollectionView)
        
        
        songsCollectionView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            songsCollectionView.topAnchor.constraint(equalTo: view.topAnchor),
            songsCollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            songsCollectionView.leftAnchor.constraint(equalTo: view.leftAnchor),
            songsCollectionView.rightAnchor.constraint(equalTo: view.rightAnchor)
        ])
        
        songsCollectionView.delegate = self
        songsCollectionView.dataSource = self
    }
    
    private func updateData() {
        interactor?.getSongList { [weak self] (data, error) in
            guard let self = self else { return }
            
            if let data = data {
                self.songList = data
                self.songsCollectionView.reloadData()
            } else {
                //Show error
            }
        }
    }
    
    private func downloadProgressHandler(downloadModel: TempModel) {
        guard let index = songList.firstIndex(where: {$0.audioURL == downloadModel.url}) else { return }
        
        let model = songList[index]
        model.downloadState = downloadModel.state
        model.progress = downloadModel.progress
        model.audioLocalPath = downloadModel.localPath
        
        songList[index] = model
        
        songsCollectionView.reloadData()
    }
    
    private func playPauseButtonHandler(song: SongViewModel) {
        switch song.playerState {
        case .play:
            audioService?.stopAudio { [weak self] success in
                guard let self = self else { return }
                
                if success {
                    self.songList.forEach { model in
                        model.playerState = .stop
                    }
                    self.songsCollectionView.reloadData()
                }
            }
        case .stop:
            audioService?.playAudio(with: song, completion: { [weak self] (success) in
                guard let self = self else { return }
                
                if success {
                    self.songList.forEach { model in
                        model.playerState = model.audioURL == song.audioURL ? .play : .stop
                    }
                    self.songsCollectionView.reloadData()
                }
            })
            
        }
    }
}

extension SongViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        songList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SongView", for: indexPath) as! SongView
        cell.configure(model: songList[indexPath.item])
        
        cell.downloadButtonHandler = { [weak self] (song) in
            guard let self = self else { return }
            
            self.interactor?.downloadSong(song)
        }
        
        cell.playPauseButtonHandler = { [weak self] (song) in
            guard let self = self else { return }
            
            self.playPauseButtonHandler(song: song)
        }
        
        return cell
    }
}
