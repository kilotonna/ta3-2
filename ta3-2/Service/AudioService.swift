import Foundation
import AVKit

final class AudioService {
    
    static let shared = AudioService()
    
    private var player: AVAudioPlayer? = nil
    
    private init() {}
    
    func playAudio(with song: SongViewModel, completion: @escaping (Bool) -> ()) {
        
        guard let audioLocalPath = song.audioLocalPath else {
            completion(false)
            return
        }
        
        if FileManager.default.fileExists(atPath: audioLocalPath.path) {
            do {
                self.player = try AVAudioPlayer(contentsOf: audioLocalPath)
                player?.prepareToPlay()
                player?.volume = 1
                player?.play()
                completion(true)
            } catch {
                print(error)
                completion(false)
            }
        }
        completion(false)
    }
    
    func stopAudio(with completion: @escaping (Bool) -> ()) {
        if let player = self.player, player.isPlaying {
            player.stop()
            completion(true)
        }
        completion(false)
    }
}
