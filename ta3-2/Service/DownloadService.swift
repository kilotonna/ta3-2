import Foundation

enum DownloadState: Int {
    case none = 0
    case progress
    case done
}

final class TempModel {
    var url: URL
    var localPath: URL?
    var state: DownloadState = .none
    var progress: Double = 0.0

    init(url: URL) {
        self.url = url
    }
}


final class DownloadService {
        
    private(set) var activeTasks = [URL: TempModel]()
    private let session: URLSession
    
    init(session: URLSession) {
        self.session = session
    }
    
    func download(_ song: SongViewModel) {
        
        let url = song.audioURL
        
        let task = session.downloadTask(with: url)
        task.resume()
        
        let downloadModel = TempModel(url: song.audioURL)
        downloadModel.state = .progress
        
        activeTasks[url] = downloadModel
    }
    
    func removeTask(by url: URL) {
        activeTasks.removeValue(forKey: url)
    }
}
