import Foundation

final class FileService {
    
    static let shared = FileService()
    
    private init() {}
    
    func localFilePath(for url: URL) -> URL? {
        guard let documentsPath = FileManager.default.urls(for: .documentDirectory,
                                                              in: .userDomainMask).first else { return nil }
        return documentsPath.appendingPathComponent(url.query ?? "")
    }

    func isFileExist(localFilePath: String) -> Bool {
        return FileManager.default.fileExists(atPath: localFilePath)
    }
}
