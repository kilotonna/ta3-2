import Foundation

enum PlayerState {
    case play
    case stop
}

final class SongViewModel {
    let id: String
    let name: String
    let audioURL: URL
    var audioLocalPath: URL?
    var downloadState: DownloadState = .none
    var progress: Double = 0.0
    var playerState: PlayerState = .stop
    
    init(id: String, name: String, audioURL: URL) {
        self.id = id
        self.name = name
        self.audioURL = audioURL
    }
}
